import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

//Components imports
import Sidebar from "./components/Sidebar";

//pages
import categoriesPage from "./pages/categories";
import categoriesAddPage from "./pages/categoriesAdd";
import itemsPage from "./pages/items";
import itemsAddPage from "./pages/itemsAdd";

//template CSS
import "./assets/plugins/nucleo/css/nucleo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "./assets/css/argon-dashboard-react.min.css";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Sidebar />
      <div className="main-content">
        <Switch>
          <Route path="/categories" exact component={categoriesPage} />
          <Route path="/categories/add" exact component={categoriesAddPage} />
          <Route path="/items" exact component={itemsPage} />
          <Route path="/items/add" exact component={itemsAddPage} />
          <Redirect from="/" to="/categories" />
        </Switch>
      </div>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
