import React, { useState } from "react";
import { Link, NavLink as NavLinkRRD } from "react-router-dom";

//ReactStrap imports
import {
  Container,
  Navbar,
  NavbarBrand,
  Nav,
  Collapse,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";

export default function Sidebar() {
  const [collapseOpen, setCollapseOpen] = useState(false);

  return (
    <Navbar
      className="navbar-vertical fixed-left navbar-light bg-white"
      expand="md"
    >
      <Container fluid>
        {/* Toggler */}
        <button
          className="navbar-toggler"
          type="button"
          onClick={() => setCollapseOpen(!collapseOpen)}
        >
          <span className="navbar-toggler-icon" />
        </button>
        {/* Logo */}
        <NavbarBrand className="pt-0 mx-auto">
          <img
            alt="logo"
            className="navbar-brand-img"
            src="https://demos.creative-tim.com/argon-dashboard-react/static/media/argon-react.f38ddea9.png"
          />
        </NavbarBrand>
        <Collapse navbar isOpen={collapseOpen}>
          <div className="navbar-collapse-header d-md-none">
            <Row>
              <Col className="collapse-brand" xs="6">
                <Link to="/">
                  <img
                    alt="..."
                    src="https://demos.creative-tim.com/argon-dashboard-react/static/media/argon-react.f38ddea9.png"
                  />
                </Link>
              </Col>
              <Col className="collapse-close" xs="6">
                <button
                  className="navbar-toggler"
                  type="button"
                  onClick={() => setCollapseOpen(!collapseOpen)}
                >
                  <span />
                  <span />
                </button>
              </Col>
            </Row>
          </div>
          {/* Heading */}
          <h6 className="navbar-heading text-muted">Category</h6>
          {/* Navigation */}
          <Nav className="mb-md-3" navbar>
            <NavItem>
              <NavLink to="/categories" tag={NavLinkRRD}>
                <i className="ni ni-spaceship" />
                All Categories
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/categories/add" tag={NavLinkRRD}>
                <i className="ni ni-palette" />
                Add New Category
              </NavLink>
            </NavItem>
          </Nav>
          {/* Divider */}
          <hr className="my-3" />
          {/* Heading */}
          <h6 className="navbar-heading text-muted">Items</h6>
          {/* Navigation */}
          <Nav className="mb-md-3" navbar>
            <NavItem>
              <NavLink to="/items" tag={NavLinkRRD}>
                <i className="ni ni-spaceship" />
                All Items
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/items/add" tag={NavLinkRRD}>
                <i className="ni ni-palette" />
                Add New Item
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
}
