import React, { useEffect, useState } from "react";

import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

export default function CustomPagination(props) {
  //states
  const [currentPage, setCurrentPage] = useState(1);

  //only runs once and call setpages
  useEffect(() => {
    setPage(1);
  }, []);

  //calculating pages
  const setPage = (page) => {
    setCurrentPage(page);
    //get last index of current
    const indexOfLast = page * props.itemPerPage;
    //get first index of current
    const indexOfFirst = indexOfLast - props.itemPerPage;
    //slice the item array to current page items
    const currentitems = props.item.slice(indexOfFirst, indexOfLast);

    //calling parent function to set current page items
    props.onItemChange(currentitems);
  };

  //calcuating total page number and store in the array
  const totalpages = Math.ceil(props.item.length / props.itemPerPage);
  const pageNumbers = [];
  for (let i = 1; i <= totalpages; i++) {
    pageNumbers.push(i);
  }

  return (
    <nav aria-label="...">
      <Pagination
        className="pagination justify-content-end mb-0"
        listClassName="justify-content-end mb-0"
      >
        <PaginationItem className={currentPage === 1 ? "disabled" : ""}>
          <PaginationLink
            onClick={(e) => setPage(currentPage - 1)}
            tabIndex="-1"
          >
            <i className="fas fa-angle-left" />
            <span className="sr-only">Previous</span>
          </PaginationLink>
        </PaginationItem>

        {pageNumbers.map((number) => {
          return (
            <PaginationItem className={number === currentPage ? "active" : ""}>
              <PaginationLink onClick={(e) => setPage(number)}>
                {number}
              </PaginationLink>
            </PaginationItem>
          );
        })}

        <PaginationItem
          className={currentPage === totalpages ? "disabled" : ""}
        >
          <PaginationLink onClick={(e) => setPage(currentPage + 1)}>
            <i className="fas fa-angle-right" />
            <span className="sr-only">Next</span>
          </PaginationLink>
        </PaginationItem>
      </Pagination>
    </nav>
  );
}
