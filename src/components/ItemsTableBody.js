import React from "react";

//Number Format
import NumberFormat from "react-number-format";

//reactstrap imports
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

export default function ItemsTableBody(props) {
  return (
    <tr>
      <td>
        <span className="mb-0 text-sm">{props.val}</span>
      </td>
      <td>Hello world</td>
      <td>Helsodadsa</td>
      <td>Helsodadsa</td>
      <td>
        <NumberFormat
          thousandSeparator={true}
          displayType={"text"}
          thousandsGroupStyle="lakh"
          prefix={"₹"}
          decimalScale={2}
          fixedDecimalScale={true}
          value={123456789}
        />
      </td>
      <td>
        <label className="custom-toggle">
          <input type="checkbox" defaultChecked />
          <span className="custom-toggle-slider rounded-circle"></span>
        </label>
      </td>
      <td className="text-right">
        <UncontrolledDropdown>
          <DropdownToggle
            className="btn-icon-only text-light"
            href="#pablo"
            role="button"
            size="sm"
            color=""
            onClick={(e) => e.preventDefault()}
          >
            <i className="fas fa-ellipsis-v" />
          </DropdownToggle>
          <DropdownMenu className="dropdown-menu-arrow">
            <DropdownItem href="#pablo" onClick={(e) => e.preventDefault()}>
              Edit
            </DropdownItem>
            <DropdownItem href="#pablo" onClick={(e) => e.preventDefault()}>
              Delete
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </td>
    </tr>
  );
}
