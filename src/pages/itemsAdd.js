import React, { useState } from "react";

//reactstrap
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  Input,
  Button,
  Alert,
} from "reactstrap";

import Header from "../components/Header";

export default function ItemsAdd() {
  //variables
  const [id, setId] = useState(0);
  const [name, setName] = useState("");
  const [category, setCategory] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [status, setStatus] = useState(false);

  const [alertVisible,setAlertVisible] = useState(false);
  const [errorMessage,setErrorMessage] = useState("")

  //dismissing alert
  const onDismiss = () => setAlertVisible(false);

  const handleFormSubmit = (e) => {
    e.preventDefault();

    //set error for category if empty
    if(category === ""){
        setAlertVisible(true);
        setErrorMessage("Please Select Category");
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }


    console.log(id, name, description, category, price, status);
  };

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <Col>
            <Card className="bg-secondary shadow">
              <CardHeader className="bg-white border-0">
                <h3 className="mb-0">Add Item</h3>
              </CardHeader>
              <CardBody>

                <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                    <Alert color="danger" isOpen={alertVisible} toggle={onDismiss}>
                        {errorMessage}
                    </Alert>
                  <FormGroup>
                    <label className="form-control-label" htmlFor="input-id">
                      ID
                    </label>
                    <Input
                      className="form-control-alternative"
                      placeholder="ID"
                      type="number"
                      name="id"
                      required
                      onChange={(e) => setId(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup>
                    <label className="form-control-label" htmlFor="input-name">
                      Name
                    </label>
                    <Input
                      className="form-control-alternative"
                      placeholder="Name"
                      type="text"
                      name="name"
                      required
                      onChange={(e) => setName(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-category"
                    >
                      Category
                    </label>
                    <Input
                      type="select"
                      onChange={(e) => setCategory(e.target.value)}
                      className="form-control-alternative"
                      name="category"
                    >
                      <option value="">Select Category</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </Input>
                  </FormGroup>
                  <FormGroup>
                    <label className="form-control-label" htmlFor="input-name">
                      Description
                    </label>
                    <Input
                      className="form-control-alternative"
                      placeholder="Description"
                      rows="3"
                      name="description"
                      type="textarea"
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-category"
                    >
                      Price
                    </label>
                    <Input
                      className="form-control-alternative"
                      placeholder="Price"
                      type="number"
                      name="price"
                      required
                      onChange={(e) => setPrice(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-status"
                    >
                      Status
                    </label>
                    <br />
                    <label className="custom-toggle">
                      <input
                        type="checkbox"
                        name="status"
                        onChange={() => setStatus(!status)}
                      />
                      <span className="custom-toggle-slider rounded-circle"></span>
                    </label>
                  </FormGroup>
                  <Button color="primary" type="submit">
                    Submit
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
