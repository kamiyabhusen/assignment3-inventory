import React, { useState } from "react";

//reactstrap
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  Input,
  Button,
} from "reactstrap";

//Components Import
import Header from "../components/Header";

export default function CategoriesAdd() {

    //variable
    const [id,setId] = useState(0);
    const [name,setName] = useState("");
    const [description,setDescription] = useState("");
    const [status,setStatus] = useState(false);

    //onsubmit from handler
    const onSubmitHandler = (e) => {
        e.preventDefault();
        console.log(id,name,description,status);
    }

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <Col>
            <Card className="bg-secondary shadow">
              <CardHeader className="bg-white border-0">
                <h3 className="mb-0">Add Category</h3>
              </CardHeader>
              <CardBody>
                <Form className="px-lg-5" onSubmit={onSubmitHandler}>
                  <FormGroup>
                    <label className="form-control-label" htmlFor="input-id">
                      ID
                    </label>
                    <Input
                      className="form-control-alternative"
                      placeholder="ID"
                      type="number"
                      onChange={(e) => setId(e.target.value)}
                      required
                    />
                  </FormGroup>
                  <FormGroup>
                    <label className="form-control-label" htmlFor="input-name">
                      Name
                    </label>
                    <Input
                      className="form-control-alternative"
                      placeholder="Name"
                      onChange={(e) => setName(e.target.value)}
                      type="text"
                      required
                    />
                  </FormGroup>
                  <FormGroup>
                    <label className="form-control-label" htmlFor="input-name">
                      Description
                    </label>
                    <Input
                      className="form-control-alternative"
                      placeholder="Description"
                      rows="3"
                      type="textarea"
                      onChange={(e) => setDescription(e.target.value)}
                      required
                    />
                  </FormGroup>
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-status"
                    >
                      Status
                    </label>
                    <br />
                    <label className="custom-toggle">
                      <input type="checkbox" onChange={(e) => setStatus(!status)} />
                      <span className="custom-toggle-slider rounded-circle"></span>
                    </label>
                  </FormGroup>
                  <Button color="primary" type="submit">
                    Submit
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
