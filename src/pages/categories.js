import React, { useState } from "react";

//reactstrap imports
import {
  Container,
  Row,
  Card,
  CardHeader,
  Table,
  CardFooter,
} from "reactstrap";

import CategoriesTableBody from "../components/CategoriesTableBody";

//components imports
import Header from "../components/Header";
import CustomPagination from "../components/Pagination";

const testArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

export default function Categories() {
  //states
  const [currentPageCategories, setCurrentPageCategories] = useState([]);

  //set current items of the page in state
  const onItemChange = (currentitem) => {
    setCurrentPageCategories(currentitem);
  };

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">All Categories</h3>
              </CardHeader>
              <Table
                className="align-items-center table-flush"
                style={{ minHeight: "200px" }}
                responsive
              >
                <thead className="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Status</th>
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                  {currentPageCategories.map((val) => {
                    return <CategoriesTableBody val={val} />;
                  })}
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <CustomPagination
                  itemPerPage={5}
                  item={testArr}
                  onItemChange={onItemChange}
                />
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
}
